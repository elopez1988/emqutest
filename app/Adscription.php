<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adscription extends Model
{
    use SoftDeletes;

    public $table = 'adscriptions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'inDate',
        'cuip',
        'category',
        'commandlevel',
        'salary',
        'compensation'
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'inDate' => 'date',
        'cuip' => 'string',
        'category' => 'string',
        'commandlevel' => 'string',
        'salary' => 'float',
        'compensation' => 'float'
    ];
}
