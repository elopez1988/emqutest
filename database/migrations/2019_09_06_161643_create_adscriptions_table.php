<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adscriptions', function (Blueprint $table) {
            $table->increments('id');

            $table->date('inDate');
            $table->string('cuip');
            $table->string('category');
            $table->string('commandlevel');
            $table->double('salary',12,2)->default(0.00);
            $table->double('compensation',12,2)->default(0.00);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adscriptions');
    }
}
